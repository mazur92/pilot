#include <alsa/asoundlib.h>
#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <limits.h>

void SetAlsaMasterVolume(long volume)
{
    long min, max;
    snd_mixer_t *handle;
    snd_mixer_selem_id_t *sid;
    const char *card = "default";
    const char *selem_name = "Master";

    snd_mixer_open(&handle, 0);
    snd_mixer_attach(handle, card);
    snd_mixer_selem_register(handle, NULL, NULL);
    snd_mixer_load(handle);

    snd_mixer_selem_id_alloca(&sid);
    snd_mixer_selem_id_set_index(sid, 0);
    snd_mixer_selem_id_set_name(sid, selem_name);
    snd_mixer_elem_t* elem = snd_mixer_find_selem(handle, sid);

    snd_mixer_selem_get_playback_volume_range(elem, &min, &max);
    snd_mixer_selem_set_playback_volume_all(elem, volume * max / 100);

    snd_mixer_close(handle);
}

int main(int argc, char *argv[]){
	char *p;
	int value;

	errno = 0;
	long conv = strtol(argv[1], &p, 10);

	// Check for errors: e.g., the string does not represent an integer
	// or the integer is larger than int
	if (errno != 0 || *p != '\0' || conv > INT_MAX) {
    	printf("Value is not a proper integer!\n");
		exit(-1);
	} else {
    // No error
    value = conv;    
	}
		
	if (argc < 2){
		printf("Usage:\npilot value\n");
	} 
	else {
		if(value >= 0 && value <= 100){ 
			SetAlsaMasterVolume(value); 
			if (value == 0){printf("Muted\n");} else if(value == 100){ printf("Volume set to max.\n");}
			else{ printf("Volume set to:%d\n", value);}
		} else { printf("Wrong value. Please enter value between 0 and 100\n");}
	}
	
return 0;
}
